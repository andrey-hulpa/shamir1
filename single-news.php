<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shamir
 */

get_header();
?>

<div style="background: linear-gradient(to bottom, rgba(84, 87, 89, 0.5), rgba(84, 87, 89, 0.5)), url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat center center / cover"
        class="single-header"></div>

    <div class="bread">
        <div class="container">
            <div class="bread-wrap">
                <a href="/" class="bread-wrap__item">Home</a>
                <div class="bread-wrap__separator">></div>
                <a href="#" class="bread-wrap__item">Professionals</a>
                <div class="bread-wrap__separator">></div>
                <a href="/news" class="bread-wrap__item">News</a>
                <div class="bread-wrap__separator">></div>
                <span class="bread-wrap__item"><?php the_title() ?></span>
            </div>
            <div class="category-wrap">
                	<?php $cat = wp_get_object_terms($post->ID, 'category');
                	if (!empty($cat)) {
                		foreach ($cat as $item) {
                			echo '<div class="category-wrap__item">' . $item->name . '</div>';
                		}
                	}
                    $tag = wp_get_object_terms($post->ID, 'tag');
                    $posttags = get_the_tags();
                    if (!empty($tag)) {
                        foreach ($tag as $item) {
                            echo '<div class="category-wrap__item">' . $item->name . '</div>';
                        }
                    }
                	 ?>
                <div class="category-wrap__share">
                    <img src="<?php echo get_template_directory_uri() ?>/img/share.svg" alt="share">
                </div>
            </div>
        </div>
    </div>

    <section class="blog-content">
        <div class="container">
            <div class="blog-content__wrap">
                <h1 class="blog-content__wrap--title" data-aos="fade-left" data-aos-duration="500" data-aos-delay="0"><?php the_title() ?></h1>
                <h2 class="blog-content__wrap--exception" data-aos="fade-right" data-aos-duration="500" data-aos-delay="0">
                    <?php the_field("exc") ?>
                </h2>
                <div class="blog-content__wrap--meta">
                    <div class="author"><?php echo get_the_author() ?></div>
                    <div class="date"><?php echo date('F j, Y', strtotime(get_the_date())) ?></div>
                </div>
                <div class="blog-content__wrap--content">
                    <?php the_content();

                    $rows = get_field('block');
                    if( $rows ) {
                        foreach( $rows as $row ) {
                            $fwidth = $row["full_width"];
                            $text = $row["text"];
                            $video = $row["video"];
                            $image = $row["image"];
                            $source = $row["source"];
                            ?>
                    <div class="blog-content__wrap--content-row" data-aos="fade-up" data-aos-duration="500" data-aos-delay="0">
                            <?php
                            if ($fwidth == 1) {
                                echo $text;
                            } else {
                                if ( $source == 'video') {?>
                                        <div class="resource">
                                            <span class="video-wrapper">
                                                <iframe src="<?php echo $video ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                                                </iframe>
                                            </span>
                                        </div>
                                    <?php
                                } elseif ( $source == 'image') {
                                    ?>
                                        <div class="resource">
                                            <img src="<?php echo $image; ?>" alt="" />
                                        </div>
                                    <?php
                                }
?>
                                <div class="text">
                                    <?php
                                    echo $text;
                                    ?>
                                </div>


                                <?php
                            }
                            ?>
                    </div>
                            <?php
                        }
                    }

                    ?>

                </div>
            </div>
            <a href="/news" class="back1">
                < Back to News</a>
        </div>
    </section>

<?php
get_footer();
