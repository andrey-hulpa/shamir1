<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shamir
 */

get_header();
?>

<div style="background: linear-gradient(to bottom, rgba(84, 87, 89, 0.5), rgba(84, 87, 89, 0.5)), url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat center center / cover"
        class="single-header"></div>

    <div class="bread">
        <div class="container">
            <div class="bread-wrap">
                <a href="/" class="bread-wrap__item">Home</a>
                <div class="bread-wrap__separator">></div>
                <a href="#" class="bread-wrap__item">Professionals</a>
                <div class="bread-wrap__separator">></div>
                <a href="/blog" class="bread-wrap__item">Blog</a>
                <div class="bread-wrap__separator">></div>
                <span class="bread-wrap__item"><?php the_title() ?></span>
            </div>
            <div class="category-wrap">
                	<?php $cat = wp_get_object_terms($post->ID, 'category');
                	if (!empty($cat)) {
                		foreach ($cat as $item) {
                			echo '<div class="category-wrap__item">' . $item->name . '</div>';
                		}
                	}
                	 ?>
                <div class="category-wrap__share">
                    <img src="<?php echo get_template_directory_uri() ?>/img/share.svg" alt="share">
                </div>
            </div>
        </div>
    </div>

    <section class="blog-content">
        <div class="container">
            <div class="blog-content__wrap">
                <h1 class="blog-content__wrap--title"><?php the_title() ?></h1>
                <h2 class="blog-content__wrap--exception">
                    <?php the_field("exc") ?>
                </h2>
                <div class="blog-content__wrap--meta">
                    <div class="author"><?php echo get_the_author() ?></div>
                    <div class="date"><?php echo date('F j, Y', strtotime(get_the_date())) ?></div>
                </div>
                <div class="blog-content__wrap--content">
                    <?php the_content() ?>
                </div>
            </div>
            <div class="blog-content__recent">
             <div class="blog-content__recent--title">We thought you may also find these interesting...</div>
            <?php 
            $categories = get_the_category($post->ID);
			if ($categories) {
				$category_ids = array();
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
				$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'showposts'=>2,
				'orderby'=>rand,
				'caller_get_posts'=>1);
				$my_query = new wp_query($args);
				if( $my_query->have_posts() ) {
					while ($my_query->have_posts()) {
					$my_query->the_post();
					?>
                <div class="blog-content__recent--item">
                    <div style="background: url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat center center / cover;" class="single-img">
                    </div>
                    <div class="single-meta">
                       <?php echo get_the_author() . " | " . date('F j, Y', strtotime(get_the_date())) ?>
                    </div>
                    <div class="single-title"><?php the_title() ?></div>
                    <div class="single-exception"><?php the_field('exc') ?>
                    </div>
                    <a  href="<?php the_permalink() ?>" class="btn btn--primary aos-init aos-animate">Read more</a>
                </div>
					<?php
				}
				}
				wp_reset_query();
			}
            ?>
            </div>
            <a href="/blog" class="back1">
                < Back to Blog</a>
        </div>
    </section>

<?php
get_footer();
