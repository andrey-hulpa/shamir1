<?php
/**
 * Template name: Front page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shamir
 */

get_header();

$hero            = get_field( 'hero_show' );
$hero_title      = get_field( 'hero_title' );
$hero_btn        = get_field( 'hero_button' );
$hero_img        = get_field( 'hero_image' );
$hero_img_mobile = get_field( 'hero_image_mobile' );

$info_show = get_field( 'info_show' );
$info      = get_field( 'info' );

$advantages        = get_field( 'advantages_show' );
$advantages_text   = get_field( 'advantages_text' );
$advantages_icons  = get_field( 'advantages_icons' );
$advantages_button = get_field( 'advantages_button' );

$technologies              = get_field( 'technologies_show' );
$technologies_text         = get_field( 'technologies_text' );
$technologies_button       = get_field( 'technologies_button' );
$technologies_image        = get_field( 'technologies_image' );
$technologies_image_mobile = get_field( 'technologies_image_mobile' );

$locations              = get_field( 'locations_show' );
$locations_text         = get_field( 'locations_text' );
$locations_button       = get_field( 'locations_button' );
$locations_image        = get_field( 'locations_image' );
$locations_image_mobile = get_field( 'locations_image_mobile' );

$categories      = get_field( 'categories_show' );
$categories_list = get_field( 'categories' );

$testimonials        = get_field( 'testimonials_show' );
$testimonials_title  = get_field( 'testimonials_title' );
$testimonials_slider = get_field( 'testimonials_slider' );

$whats_new       = get_field( 'whats_new_show' );
$whats_new_title = get_field( 'whats_new_title' );
$whats_new_items = get_field( 'whats_new_items' );

?>
<?php if ( $hero ) : ?>
	<div class="section section--hero section--white d-flex align-items-center">
		<?php
		$class = '';
		if ( $hero_img_mobile ) {
			$class = 'd-none d-md-block ';
			$img   = wp_get_attachment_image(
				$hero_img_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $hero_img_mobile['title'],
					'alt'   => $hero_img_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $hero_img ) {
			$img = wp_get_attachment_image(
				$hero_img['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $hero_img['title'],
					'alt'   => $hero_img['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container text-center position-relative">
			<?php if ( $hero_title ) : ?>
				<div data-aos-delay="1000" data-aos-duration="1500" data-aos="zoom-in">
					<?php _e( $hero_title, '_s' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( $hero_btn ) : ?>
				<a data-aos-delay="3000" data-aos="fade-in" data-aos-duration="500"
				   target="<?php esc_attr_e( $hero_btn['target'], '_s' ); ?>"
				   href="<?php echo esc_url( $hero_btn['url'] ); ?>"
				   title="<?php esc_attr_e( $hero_btn['title'], '_s' ); ?>"
				   class="btn btn--primary"><?php _e( $hero_btn['title'] ); ?></a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<?php if ( $info_show ) : ?>
	<div class="section section--info section--white section--red">
		<div class="container" data-aos-delay="3000" data-aos-duration="1500" data-aos="fade-up">
			<?php _e( $info, '_s' ); ?>
		</div>
	</div>
<?php endif; ?>
<?php if ( $advantages ) :
	$delay = 0;
	?>
	<div class="section section--advantages advantages">
		<div class="container">
			<?php if ( $advantages_text ) : ?>
				<div data-aos="fade-up" data-aos-duration="500" data-aos-delay="<?php echo $delay; ?>">
					<?php _e( $advantages_text, '_s' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( $advantages_icons ) : ?>
				<div class="row advantages__row">
					<?php foreach ( $advantages_icons as $item ) :
						$icon = $item['icon'];
						$title = $item['title'];
						$text = $item['text'];
						$delay += 300;
						?>
						<div class="col-6 col-xl-3 ml-auto" data-aos-delay="<?php echo $delay; ?>"
						     data-aos="zoom-in">
							<div class="advantages__item">
								<div class="advantages__icon-wrap">
									<?php
									if ( $icon ) {
										$img = wp_get_attachment_image(
											$icon['id'],
											'section',
											false,
											array(
												'class' => 'advantages__icon',
												'title' => $icon['title'],
												'alt'   => $icon['alt'],
											)
										);
										echo $img;
									}
									?>
								</div>
								<div class="advantages__title">
									<?php _e( $title ); ?>
								</div>
								<div class="advantages__text">
									<?php _e( $text ); ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="text-center">
				<?php if ( $advantages_button ) :
					$delay += 300;
					?>
					<a data-aos="fade-up" data-aos-duration="500" data-aos-delay="<?php echo $delay; ?>"
					   target="<?php esc_attr_e( $advantages_button['target'], '_s' ); ?>"
					   href="<?php echo esc_url( $advantages_button['url'] ); ?>"
					   title="<?php esc_attr_e( $advantages_button['title'], '_s' ); ?>"
					   class="btn btn--primary"><?php _e( $advantages_button['title'] ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $technologies ) :
	$delay = 0;
	?>
	<div class="section section--technologies section--white technologies d-md-flex align-items-center">
		<?php
		$class = '';
		if ( $technologies_image_mobile ) {
			$class = ' d-none d-md-block ';
			$img   = wp_get_attachment_image(
				$technologies_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $technologies_image_mobile['title'],
					'alt'   => $technologies_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $technologies_image ) {
			$img = wp_get_attachment_image(
				$technologies_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $technologies_image['title'],
					'alt'   => $technologies_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container text-center text-lg-left">
			<div class="row">
				<div class="col-12 col-lg-6 offset-lg-6">
					<div class="technologies__container">
						<?php if ( $technologies_text ) : ?>
							<div data-aos-duration="1500" data-aos="fade-left">
								<?php _e( $technologies_text, '_s' ); ?>
							</div>
						<?php endif; ?>
						<?php if ( $technologies_button ) :
							$delay += 1000; ?>
							<a data-aos-delay="<?php echo $delay; ?>" data-aos="fade-up" data-aos-duration="500"
							   target="<?php esc_attr_e( $technologies_button['target'], '_s' ); ?>"
							   href="<?php echo esc_url( $technologies_button['url'] ); ?>"
							   title="<?php esc_attr_e( $technologies_button['title'], '_s' ); ?>"
							   class="btn btn--primary"><?php _e( $technologies_button['title'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $locations ) :
	$delay = 0;
	?>
	<div class="section section--locations locations d-flex align-items-center">
		<?php
		$class = '';
		if ( $locations_image_mobile ) {
			$class = ' d-none d-md-block ';
			$img   = wp_get_attachment_image(
				$locations_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $locations_image_mobile['title'],
					'alt'   => $locations_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $locations_image ) {
			$img = wp_get_attachment_image(
				$locations_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $locations_image['title'],
					'alt'   => $locations_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container text-center text-lg-left">
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="locations__container">
						<?php if ( $locations_text ) : ?>
							<div data-aos-duration="1500" data-aos="fade-right">
								<?php _e( $locations_text, '_s' ); ?>
							</div>
						<?php endif; ?>
						<?php if ( $locations_button ) :
							$delay += 1000; ?>
							<a data-aos-delay="<?php echo $delay; ?>" data-aos="fade-up" data-aos-duration="500"
							   target="<?php esc_attr_e( $locations_button['target'], '_s' ); ?>"
							   href="<?php echo esc_url( $locations_button['url'] ); ?>"
							   title="<?php esc_attr_e( $locations_button['title'], '_s' ); ?>"
							   class="btn btn--primary"><?php _e( $locations_button['title'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $categories && $categories_list ) : ?>
	<div class="categories">
		<?php foreach ( $categories_list as $item ) :
			$title = $item['new_title'];
			$link = $item['link'];
			$image = $item['image'];
			?>
			<a href="<?php echo esc_url( $link['url'] ); ?>" class="categories__item">
				<?php
				if ( $image ) {
					$img = wp_get_attachment_image(
						$image['id'],
						'categories_img',
						false,
						array(
							'class' => 'categories__img',
							'title' => $image['title'],
							'alt'   => $image['alt'],
						)
					);
					echo $img;
				}
				?>
				<?php if ( $title ) : ?>
					<div class="categories__title">
						<?php _e( $title ); ?>
					</div>
				<?php endif; ?>
			</a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
<?php get_template_part('template-parts/section', 'pro'); ?>
<?php if ( $testimonials ) : ?>
	<div class="section section--testimonials testimonials">
		<div class="container position-relative">
			<?php if ( $testimonials_title ) : ?>
				<?php _e( $testimonials_title, '_s' ); ?>
			<?php endif; ?>
			<?php if ( $testimonials_slider ) : ?>
				<div class="swiper-container testimonials__swiper">
					<div class="swiper-wrapper">
						<?php foreach ( $testimonials_slider as $item ) :
							$image = $item['image'];
							$text = $item['text'];
							$author = $item['author'];
							?>
							<div class="swiper-slide testimonials__slide">
								<?php if ( $image ) : ?>
									<div class="testimonials__img-wrap">
										<?php
										$img = wp_get_attachment_image(
											$image['id'],
											'section',
											false,
											array(
												'class' => 'testimonials__img',
												'title' => $image['title'],
												'alt'   => $image['alt'],
											)
										);
										?>
										<?php echo $img; ?>
										<div class="swiper-lazy-preloader"></div>
									</div>
								<?php endif; ?>
								<div class="testimonials__content">
									<blockquote>
										<?php _e( $text, '_s' ); ?>
									</blockquote>
									<div class="testimonials__author">
										<?php _e( $author, '_s' ); ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="swiper-pagination testimonials__pagination"></div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<?php if ( $whats_new ) : ?>
	<div class="section section--whats-new whats-new">
		<div class="container">
			<?php if ( $whats_new_title ) : ?>
				<?php _e( $whats_new_title, '_s' ); ?>
			<?php endif; ?>
			<?php if ( $whats_new_items ) :
				$class = 'ml-auto col-12 ';
				$add_class = 'col-md-4 ';
				?>
				<div class="row">
					<?php foreach ( $whats_new_items as $item ) :
						$image = $item['image'];
						$image_mobile = $item['image_mobile'];
						$name = $item['name'];
						$title = $item['title'];
						$excerpt = $item['excerpt'];
						$button = $item['button'];
						?>
						<div class="<?php echo $class . $add_class; ?>">
							<div class="whats-new__item">
								<?php if ( $image || $image_mobile ) : ?>
									<a href="<?php echo $button['url'] ?>"
									   title="<?php _e( $title ); ?>" class="whats-new__img-wrap">
										<?php
										$class_img = '';
										if ( $image_mobile ) {
											$class_img = ' d-none d-md-block ';
											$img       = wp_get_attachment_image(
												$image_mobile['id'],
												'whats-new__img-mobile',
												false,
												array(
													'class' => 'whats-new__img whats-new__img--mobile d-block d-md-none',
													'title' => $image_mobile['title'],
													'alt'   => $image_mobile['alt'],
												)
											);
											echo $img;
										}
										if ( $image ) {
											$img = wp_get_attachment_image(
												$image['id'],
												'whats-new__img',
												false,
												array(
													'class' => $class_img . 'whats-new__img',
													'title' => $image['title'],
													'alt'   => $image['alt'],
												)
											);
											echo $img;
										}
										?>
									</a>
								<?php endif; ?>
								<?php if ( $name ) : ?>
									<div class="whats-new__headline"><?php _e( $name ); ?></div>
								<?php endif; ?>
								<?php if ( $title ) : ?>
									<a href="<?php echo $button['url'] ?>"
									   title="<?php _e( $title ); ?>" class="whats-new__title">
										<?php _e( $title ) ?>
									</a>
								<?php endif; ?>
								<?php if ( $excerpt ) : ?>
									<div class="whats-new__excerpt">
										<?php _e( $excerpt ) ?>
									</div>
								<?php endif; ?>
								<?php if ( $button ) : ?>
									<a target="<?php esc_attr_e( $button['target'], '_s' ); ?>"
									   href="<?php echo esc_url( $button['url'] ); ?>"
									   title="<?php esc_attr_e( $button['title'], '_s' ); ?>"
									   class="btn btn--primary"><?php _e( $button['title'] ); ?></a>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<?php if ( $subscribe ) :
	?>
	<div class="section section--subscribe section--white subscribe section--red">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-4 d-flex align-items-center justify-content-md-start justify-content-center">
					<?php if ( $subscribe_title ) : ?>
						<?php _e( $subscribe_title, '_s' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-md-6 col-lg-8 flex-column flex-md-row d-flex align-items-center justify-content-md-end justify-content-center">
					<?php if ( $subscribe_text ) : ?>
						<?php _e( $subscribe_text, '_s' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $partner ) :
	?>
	<div class="section section--partner section--white partner">
		<?php
		$class = '';
		if ( $partner_image_mobile ) {
			$class = ' d-none d-md-block ';
			$img   = wp_get_attachment_image(
				$partner_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $partner_image_mobile['title'],
					'alt'   => $partner_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $partner_image ) {
			$img = wp_get_attachment_image(
				$partner_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $partner_image['title'],
					'alt'   => $partner_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-5">
					<?php if ( $partner_text ) : ?>
						<?php _e( $partner_text, '_s' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php
get_footer();
