<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shamir
 */

get_header();
?>

<div style="background: linear-gradient(to bottom, rgba(84, 87, 89, 0.5), rgba(84, 87, 89, 0.5)), url(<?php echo get_template_directory_uri() . "/img/q3.png" ?>) no-repeat center center / cover"
        class="single-header single-header__blog">
        	<div class="container">
        		<div class="single-header__title">
        		Shamir Professional blog
        	</div>
        	</div>
        </div>

    <div class="bread">
        <div class="container">
            <div class="bread-wrap">
                <a href="/" class="bread-wrap__item">Home</a>
                <div class="bread-wrap__separator">></div>
                <a href="/blog" class="bread-wrap__item">Blog</a>
                <div class="bread-wrap__separator">></div>
                <span class="bread-wrap__item"><?php echo single_cat_title() ?></span>
            </div>
            <div class="category-wrap">
            	<div class="category-wrap__hot">Hot Topics #</div>
                	<?php 
                	$cat = get_terms(['taxonomy' => 'category']);
                	if (!empty($cat)) {
                		foreach ($cat as $item) {
                			echo '<a href="/category/' . $item->slug . ' " class="category-wrap__item">' . $item->name . '</a>';
                		}
                	}
                	 ?>
            </div>
        </div>
    </div>

    <section class="blog-content">
        <div class="container">
            <div class="blog-content__recent">



		<?php if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				?>
									<div class="blog-content__recent--item blog-page__item">
                    <div class="longer"><div style="background: url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat center center / cover;" class="single-img">
                    </div></div>
                    <div class="longer2">
                    	<div class="single-meta">
                       <?php echo get_the_author() . " | " . date('F j, Y', strtotime(get_the_date())) ?>
                    </div>
                    <div class="single-title"><?php the_title() ?></div>
                    <div class="single-exception"><?php the_field('exc') ?>
                    </div>
                    <a  href="<?php the_permalink() ?>" class="btn btn--primary aos-init aos-animate">Read more</a>
                    </div>
                </div>
				<?php

			endwhile;


		endif;
		?>

        </div>
    </section>

<?php
get_footer();
