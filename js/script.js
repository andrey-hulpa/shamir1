$(function() {

    $('.js-lang').on('click', function () {
        $(this).toggleClass('red');
        $(this).closest('i').toggleClass('rotate');
        $( '.js-lang-body' ).slideToggle('500');
    });

    $('.nav').on('click', '.nav-menu > .menu-item > a', function (event) {
        if (window.outerWidth <= 992) {
            event.preventDefault();
        }
    });

    document.querySelectorAll('.nav .menu a').forEach(el => {
        let i = document.createElement('i');
        i.className = 'fas fa-angle-right d-block d-lg-none';
        el.appendChild(i);
    });

    AOS.init();
    $('.lazy').Lazy();

    if ( document.querySelector('.testimonials__swiper') ) {
        let testimonialsSwiper = new Swiper ('.testimonials__swiper', {
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                bulletClass: 'testimonials__bullet',
                bulletActiveClass: 'testimonials__bullet--active',
                clickable: true
            },
            lazy: {
                elementClass: 'lazy'
            }
        });
    };

    if (document.querySelector('.js-tabs')) {
        let markersGroup = {
        	start: 0,
	        end: 0
        };
        let markers = [];
        let map = '';

        $( ".js-tabs" ).tabs({
            activate: function( event, ui ) {
                $(".js-accordion",ui.newPanel).accordion("refresh");
	            let counter = $(".js-accordion",ui.newPanel).children('.accordion__title').first().data('counter');
	            for (let i = 0; i < markers.length; i++) {
		            const element = markers[i];
		            element.setIcon('../wp-content/themes/shamir/img/icon-loc-inactive.svg');
	            }
	            markers[counter].setIcon('../wp-content/themes/shamir/img/icon-loc.svg');
            }
        });
        $( ".js-tabs" ).on( "tabsactivate", function( event, ui ) {
            $(".js-accordion",ui.newPanel).accordion("refresh");
            markersGroup.start = $(".js-accordion",ui.newPanel).children('.accordion__title').first().data('counter');
            markersGroup.end = $(".js-accordion",ui.newPanel).children('.accordion__title').last().data('counter');
            centerMap(map, markersGroup);
        } );
        $( ".js-accordion" ).accordion();
        $( ".accordion__title" ).on('click', function( ) {
            let counter = $(this).data('counter');
            if (counter < markers.length) {
                for (let i = 0; i < markers.length; i++) {
                    const element = markers[i];
                    element.setIcon('../wp-content/themes/shamir/img/icon-loc-inactive.svg');
                }
                markers[counter].setIcon('../wp-content/themes/shamir/img/icon-loc.svg');
            }
        });
        /**
         * initMap
         *
         * Renders a Google Map onto the selected jQuery element
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @return  object The map instance.
         */
        function initMap( $el ) {

            // Find marker elements within map.
            var $markers = $el.find('.marker');

            // var style = [
            //     {
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#ededed"
            //             }
            //         ]
            //     },
            //     {
            //         "elementType": "labels",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "elementType": "labels.icon",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#616161"
            //             }
            //         ]
            //     },
            //     {
            //         "elementType": "labels.text.stroke",
            //         "stylers": [
            //             {
            //                 "color": "#f5f5f5"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "administrative",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "administrative.land_parcel",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "administrative.land_parcel",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#bdbdbd"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "administrative.neighborhood",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "poi",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "poi",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#eeeeee"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "poi",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#757575"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "poi.park",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#e5e5e5"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "poi.park",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#9e9e9e"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#ffffff"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road",
            //         "elementType": "labels.icon",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road.arterial",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#757575"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road.highway",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#dadada"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road.highway",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#616161"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "road.local",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#9e9e9e"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "transit",
            //         "stylers": [
            //             {
            //                 "visibility": "off"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "transit.line",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#e5e5e5"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "transit.station",
            //         "elementType": "geometry",
            //         "stylers": [
            //             {
            //                 "color": "#eeeeee"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "water",
            //         "stylers": [
            //             {
            //                 "color": "#ffffff"
            //             }
            //         ]
            //     },
            //     {
            //         "featureType": "water",
            //         "elementType": "labels.text.fill",
            //         "stylers": [
            //             {
            //                 "color": "#9e9e9e"
            //             }
            //         ]
            //     }
            // ];

            // Create gerenic map.
            var mapArgs = {
                zoom        : 9,
                mapTypeId   : google.maps.MapTypeId.ROADMAP,
                // styles: style,
                disableDefaultUI: true,
                center: {lat: 33.166754, lng: 35.659949},
            };
            map = new google.maps.Map( $el[0], mapArgs );

            // Add markers.
            map.markers = [];
            $markers.each(function(){
                initMarker( $(this), map );
            });

            // Center map based on markers.
            // centerMap( map );

            // Return map instance.
            return map;
        }
        /**
         * initMarker
         *
         * Creates a marker for the given jQuery element and map.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @param   object The map instance.
         * @return  object The marker instance.
         */
        function initMarker( $marker, map ) {
            // Get position from marker.
            var lat = $marker.data('lat');
            var lng = $marker.data('lng');
            var latLng = {
                lat: parseFloat( lat ),
                lng: parseFloat( lng )
            };
            const image =
                "../wp-content/themes/shamir/img/icon-loc-inactive.svg";
            // Create marker instance.
            var marker = new google.maps.Marker({
                position : latLng,
                map: map,
                icon: image
            });
            let position = markers.length;
            markers.push(marker);
            // Append to reference for later use.
            map.markers.push( marker );
            google.maps.event.addListener(marker, 'click', function() {
                for (let i = 0; i < markers.length; i++) {
                    const element = markers[i];
                    element.setIcon('../wp-content/themes/shamir/img/icon-loc-inactive.svg');
                }
                markers[position].setIcon('../wp-content/themes/shamir/img/icon-loc.svg');

                let parentAccordion = $( "[data-counter='"+ (position-1) +"']" ).parent(".js-accordion");
                let first = $(parentAccordion).find('.accordion__title').first().data("counter");
                $( parentAccordion ).accordion( "option", "active", first != -1 ? position - first : position );

                let tabActive = $( parentAccordion ).parent();
                let tabActiveIndex = $(".contact__tab").index(tabActive);
                $( tabActive ).closest(".js-tabs").tabs( "option", "active", tabActiveIndex );
            });
            // If marker contains HTML, add it to an infoWindow.
            if( $marker.html() ){

                // Create info window.
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // Show info window when marker is clicked.
                google.maps.event.addListener(marker, 'mouseover', function() {
                    for (let i = 0; i < markers.length; i++) {
                        const element = markers[i];
                        element.setIcon('../wp-content/themes/shamir/img/icon-loc-inactive.svg');
                    }
                    marker.setIcon('../wp-content/themes/shamir/img/icon-loc.svg');
                    infowindow.open( map, marker );
                });
                // Show info window when marker is clicked.
                google.maps.event.addListener(marker, 'mouseout', function() {
                    // marker.setIcon('../wp-content/themes/shamir/img/icon-loc-inactive.svg');
                    infowindow.close();
                });
            }
        }

        /**
         * centerMap
         *
         * Centers the map showing all markers in view.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   object The map instance.
         * @return  void
         */
        var bounds = new google.maps.LatLngBounds();
        function centerMap( _map, _markersGroup ) {
            // Create map boundaries from all map markers.
	        bounds = new google.maps.LatLngBounds();
            _map.markers.forEach(function( marker, index ){
                if ( index >= _markersGroup.start && index <= _markersGroup.end )
                bounds.extend({
                    lat: marker.position.lat(),
                    lng: marker.position.lng()
                });
            });

            // Case: Single marker.
            if( _map.markers.length == 1 ){
                _map.fitBounds( bounds.getCenter() );
                // Case: Multiple markers.
            } else{
                if ( _markersGroup.start == _markersGroup.end ) {
                    _map.panTo( bounds.getCenter() );
                    _map.setZoom(9);
                }
                else {
                    _map.fitBounds( bounds );
                }
            }
        }

        $('.acf-map').each(function(){
            var map = initMap( $(this) );
        });
    }
    if ( '.technologies' ) {
        function getHashFilter() {
            // get filter=filterName
            var matches = location.hash.match( /filter=([^&]+)/i );
            var hashFilter = matches && matches[1];
            return hashFilter && decodeURIComponent( hashFilter );
        }

        var $grid = $('.technologies__grid');

        // bind filter button click
        $('.js-technologies__filter').on( 'change', function() {
            let filterAttr = $(this).val();
            location.hash = 'filter=' + encodeURIComponent( filterAttr );
        });

        var isIsotopeInit = false;

        function onHashchange() {
            var hashFilter = getHashFilter();
            if ( !hashFilter && isIsotopeInit ) {
                return;
            }
            isIsotopeInit = true;
            // filter isotope
            $grid.isotope({
                percentPosition: true,
                itemSelector: '.technologies__item',
                layoutMode: 'fitRows',
                filter: hashFilter
            });
        }

        $(window).on( 'hashchange', onHashchange );
        // trigger event handler to init Isotope
        onHashchange();
    }
    if ($('.page-template-archive-news').length) {
        var i = 0,
            l = 2,
            s = 0;

        // $('.news-content__recent .row:last-child').remove()
        $('.news-content__recent .row.odd .news-page__item.template--2').each(function (index) {
            if($(this).parent().find('.news-page__item').length > 2) {
                var inet = $(this).parent().find('.news-page__item').not('.template--2').get(0);
                inet.remove();
                if ($(this).parent().prev().is('.even')) {
                    $(this).parent().addClass('odd')
                } else {
                    $(this).parent().addClass('even')
                }
                // $(this).parent().parent().append( '<div class="row created"> </div>' )
                // $(this).parent().parent().find('.row.created').append( inet )
            } if ($(this).parent().find('.news-page__item').length == 2) {
                $(this).parent().find('.news-page__item').not('.template--2').addClass('LOL')
            }
        })
        $('.news-content__recent .row.odd .news-page__item').each(function (index) {
            if (index == 0) {
                $(this).addClass('LOL')
            }
        })
        if ($('.news-content__recent .row').find('.template--2') == 1) {
            if ($('.news-content__recent .row:last-child').length < 1) {
                $('.news-content__recent .row.even .news-page__item').not('.template--2').addClass('LOL')
            }
        }
        $('.news-content__recent .row.even .news-page__item').each(function (index) {
            $(this).addClass(index)
            if (index == 0) {
                $(this).addClass('LOL')
            }
        })
        var elem = document.querySelector('.news-content__recent .row');

        // var l = $('.news-page__item').length;
        // $('.news-content__recent .news-page__item').not('.template--2').each(function (index) {
        //     array.push(this)
        // })
        // $('.news-content__recent .news-page__item.template--2').each(function (index) {
        //     array2.push(this)
        // })
        let arr = document.querySelectorAll('.news-page__item');
        // console.log(arr);
        let lolArr = [];
        let baseArr = [];
        let finalyArr = [];
        arr.forEach(element => { if(element.classList.contains('template--2')) {
            console.log(lolArr)
            lolArr.push(element);
        } else {
            baseArr.push(element);
        }
        });
        function shuffle(array) {
            /* array.sort(() => Math.random() - 0.5); */
        }


        function foo() {
            while(baseArr.length > 0) {
                if(lolArr.length > 0) {
                    finalyArr.push([baseArr.pop(), lolArr.pop()]);
                }
                else {
                    finalyArr.push([baseArr.pop(), baseArr.pop(), baseArr.pop()]);
                }
            }
            shuffle(finalyArr);
        }
        foo();
        var o = 0;
        var v = 2;
        while(finalyArr.length > 0) {
            $('.news-content__recent').append('<div class="row"></div>')
            while (finalyArr[v][o]) {
                console.log(finalyArr[v][o])
                $('.news-content__recent .row:last-child').append(
                    finalyArr[v][o]
                )
                o++;
            }
            o=0;
            v--;
            finalyArr.length--;
        }


        $('.news-content__recent .row').each(function (index) {
            if (index == i) {
                $(this).addClass('odd')
                $(this).find('.news-page__item:first-child').addClass('LOL')
                i+=2;
            } else {
                $(this).addClass('even')
                $(this).find('.news-page__item:first-child').addClass('LOL')
            }
        })
setTimeout(function () {
        if (window.innerWidth > 991) {
            var widtg = $('.news-page__item').not('.LOL').width()
            $('.news-content__recent .row.even').each(function (index) {
                new Masonry( this, {
                    columnWidth: widtg,
                    itemSelector: '.news-page__item',
                    originLeft: false,
                    fitWidth: true,
                    gutter: 20,
                });
            })
            $('.news-content__recent .row.odd').each(function (index) {
                new Masonry( this, {
                    // options
                    columnWidth: widtg,
                    itemSelector: '.news-page__item',
                    // percentPosition: true,
                    fitWidth: true,
                    gutter: 20,
                });
            })
        }
}, 1000)

        $(window).resize(function () {
            if (window.innerWidth > 991) {
                var widtg = $('.news-page__item').not('.LOL').width()
                $('.news-content__recent .row.even').each(function (index) {
                    new Masonry( this, {
                        columnWidth: widtg,
                        itemSelector: '.news-page__item',
                        originLeft: false,
                        fitWidth: true,
                        gutter: 20,
                    });
                })
                $('.news-content__recent .row.odd').each(function (index) {
                    new Masonry( this, {
                        // options
                        columnWidth: widtg,
                        itemSelector: '.news-page__item',
                        // percentPosition: true,
                        fitWidth: true,
                        gutter: 20,
                    });
                })
            }
        })
    }
    if ($('.video-wrapper').length) {
        var videoPlayButton,
            videoWrapper = document.getElementsByClassName('video-wrapper')[0],
            video = document.getElementsByTagName('video')[0],
            videoMethods = {
                renderVideoPlayButton: function() {
                    if (videoWrapper.contains(video)) {
                        this.formatVideoPlayButton()
                        video.classList.add('has-media-controls-hidden')
                        videoPlayButton = document.getElementsByClassName('video-overlay-play-button')[0]
                        videoPlayButton.addEventListener('click', this.hideVideoPlayButton)
                    }
                },

                formatVideoPlayButton: function() {
                    videoWrapper.insertAdjacentHTML('beforeend', '\
                <span class="video-overlay-play-button" >\
                </span>\
            ')
                },

                hideVideoPlayButton: function() {
                    video.play()
                    videoPlayButton.classList.add('is-hidden')
                    video.classList.remove('has-media-controls-hidden')
                    video.setAttribute('controls', 'controls')
                }
            }

        videoMethods.renderVideoPlayButton()
    }
    if ( document.querySelector('.technologies__swiper') ) {
        $('.technologies__swiper .technologies__item').each((index, el) => {
            $(el).addClass('swiper-slide');
        });
        let technologiesSwiper = new Swiper ('.technologies__swiper', {
            loop: true,
            slidesPerView: 1,
            pagination: {
                el: '.technologies__pagination',
                bulletClass: 'technologies__bullet',
                bulletActiveClass: 'technologies__bullet--active',
                clickable: true
            },
            lazy: {
                elementClass: 'lazy'
            },
            navigation: {
            nextEl: '.swiper-button-next1',
            prevEl: '.swiper-button-prev1',
          },
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
          },
          breakpoints: {
            991: {
              slidesPerView: 2,
            }
          }
        });
    };
});

document.addEventListener("DOMContentLoaded", () => {
    function testWebP(callback) {

        var webP = new Image()
        webP.onload = webP.onerror = function () {
            callback(webP.height == 2)
        }
        webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA"
    }

    testWebP(function (support) {

        if (support == true) {
            document.querySelector('body').classList.add('webp')
        }
    })

})