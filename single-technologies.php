<?php
get_header( 'single-technologies' );
$technologies_arguments = array(
	'post_type'      => 'technologies',
	'posts_per_page' => - 1
);

$technologies = new WP_Query( $technologies_arguments );
?>

<?php 


$tech_description = get_field( 'technology_description' );
$tech_background = get_field( 'technology_background' );

$benefits_title = get_field( 'benefits_title' );
$benefits_icons = get_field( 'benefits_icons' );
$benefits_button = get_field( 'benefits_button' );

$howitworks_text_and_image = get_field( 'howitworks_text_and_image' );

?>
<div class="tech-block1-wrapper">
<div class="container">

    <div style="margin-top: 3em" class="bread">
        <div class="container">
            <div class="bread-wrap">
                <a href="/" class="bread-wrap__item">Home</a>
                <div class="bread-wrap__separator">></div>
                <a href="#" class="bread-wrap__item">Professionals</a>
                <div class="bread-wrap__separator">></div>
                <a href="/technologies" class="bread-wrap__item">Technologies</a>
                <div class="bread-wrap__separator">></div>
                <span class="bread-wrap__item"><?php the_title() ?></span>
            </div>
        </div>
    </div>
<!--tech block-->

	<div class="tech_description">
		
	<div data-aos-once="true" data-aos-duration="1500" data-aos-delay="1000" data-aos="fade-right"><?php _e($tech_description, 's'); ?></div>

<?php if( !empty( $tech_background ) ): ?>
    <img class="tech_background" src="<?php echo esc_url($tech_background['url']); ?>" alt="<?php echo esc_attr($tech_background['alt']); ?>" />
<?php endif; ?>
	</div>
</div>
</div> <!-- close container div for full width -->



<!--benefits block-->

<div class="tech-benefits-wrapper">


<h2><?php _e($benefits_title, 's'); ?></h2>

<?php if( !empty( $benefits_icons ) ): ?>
<div class="tech-benefits-block">
<?php foreach ($benefits_icons as $icons){ ?>
    <div class="tech-benefits-icons">
	<img class="benefits_icons" src="<?php echo esc_url($icons['benefits_icon_image']['url']); ?>" alt="<?php echo esc_attr($icons['benefits_icon_image']['alt']); ?>" />
	<h3 data-aos-once="true" data-aos-duration="1500" data-aos="fade-up"><?php  _e($icons['benefits_icon_title']); ?></h3>
	<span>
	<?php  _e($icons['benefits_icon_text']); ?>
	</span>
	</div>
<?php } ?>
</div>
<?php endif; ?>

<?php if ( $benefits_button ) :
$delay += 300;
?>
<a data-aos="fade-up" data-aos-duration="500" data-aos-delay="<?php echo $delay; ?>"
target="<?php esc_attr_e( $benefits_button['target'], '_s' ); ?>"
href="<?php echo esc_url( $benefits_button['url'] ); ?>"
title="<?php esc_attr_e( $benefits_button['title'], '_s' ); ?>"
class="btn btn--primary"><?php _e( $benefits_button['title'] ); ?></a>
<?php endif; ?>

</div>

<div class="container"><!-- open container div for full width -->
<!--how it works-->
	<div  class="howitworks">
	<?php
	if ($howitworks_text_and_image){
		foreach ($howitworks_text_and_image as $item){ ?>
		
		<div data-aos-once="true" data-aos-duration="1500" data-aos="fade-left" class="howitworks_description"><?php _e($item['howitworks_description']); ?></div>
		<div data-aos-once="true" data-aos-duration="2000" data-aos="fade-right" class="howitworks_image"><img class="benefits_icons" src="<?php echo esc_url($item['howitworks_image']['url']); ?>" alt="<?php echo esc_attr($item['howitworks_image']['alt']); ?>" /></div>
		<?php	
		}
	}
	?>
	</div>

</div>

<?php get_template_part('template-parts/section', 'pro'); ?>


<div class="container">
<!-- more tech scroll -->
<div class="more_technologies_scroll">
	<h2 class="text-center">View more technologies</h2>
	<div class="technologies__swiper swiper-container custom-swiper1">
	<div class="swiper-button-prev1 custom-btn1"></div>
	<div class="swiper-button-next1 custom-btn1"></div>
		<div class="swiper-wrapper">
<?php
if ( $technologies->have_posts() ):
	while ( $technologies->have_posts() ): $technologies->the_post();
		?>
		<?php
		if(stristr($_SERVER['REQUEST_URI'], $post->post_name)===false){
		get_template_part('template-parts/technologies-item');
		}
		 ?>
	<?php
	endwhile;
	wp_reset_postdata();
endif; ?>
		</div>
		<div class="swiper-pagination technologies__pagination">	</div>
	</div>
</div>

		
		<br /><br />		
		<br /><br />
		
</div>
<?php
get_footer();
?>


