<div  class="technologies__item <?php echo strtolower( str_replace( '™', '', str_replace( ' ', '', get_the_title() ) ) ); ?>">
	<div data-aos-offset="-300" data-aos-once="true" data-aos-duration="1500" data-aos="fade-right" class="technologies__img-wrap">
		<a href="<?php the_permalink(); ?>" class="technologies__link">
			<?php the_post_thumbnail(); ?>
		</a>
	</div>
	<div class="technologies__content">
		<h3 class="technologies__title">
			<a href="<?php the_permalink(); ?>" class="technologies__link">
				<?php the_title(); ?> >
			</a>
		</h3>
		<a href="<?php the_permalink() ?>" class="technologies__link">
			<?php the_excerpt(); ?>
		</a>
	</div>
</div>