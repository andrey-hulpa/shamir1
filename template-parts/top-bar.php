<div class="top-bar">
	<div class="container d-flex justify-content-end align-items-center">
		<?php
		wp_nav_menu(
			array(
				'theme_location'  => 'menu-top-bar',
				'menu_id'         => 'top-bar-menu',
				'container_class' => 'top-bar__nav',
				'menu_class'      => 'top-bar__list'
			)
		);
		?>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'lang',
				'depth'          => 0
			)
		);
		?>
		<div class="js-lang-body lang top-bar__language-dropdown">
			<div class="container">
				<div class="lang__container">
					<?php
					$title          = get_field( 'lang_title', 'options' );
					$lang_link_icon = get_field( 'lang_link_icon', 'options' );
					$lang_link      = get_field( 'lang_link', 'options' );
					$lang_group     = get_field( 'lang_group', 'options' );
					?>
					<?php if ( $title ) : ?>
						<div class="lang__title">
							<?= $title; ?>
						</div>
					<?php endif; ?>
					<?php if ( $lang_link ) : ?>
						<a target="<?php esc_attr_e( $lang_link['target'], '_s' ); ?>"
						   href="<?php echo esc_url( $lang_link['url'] ); ?>"
						   title="<?php esc_attr_e( $lang_link['title'], '_s' ); ?>"
						   class="lang__btn">
							<?php
							if ( $lang_link_icon ) {
								$img = wp_get_attachment_image(
									$lang_link_icon['id'],
									'lang_link_icon',
									false,
									array(
										'class' => 'lang__btn-icon',
										'title' => $lang_link_icon['title'],
										'alt'   => $lang_link_icon['alt'],
									)
								);
								echo $img;
							}
							?>
							<?php _e( $lang_link['title'] ); ?>
						</a>
					<?php endif; ?>
					<?php if ( $lang_group ) : ?>
						<div class="lang__group">
							<?php
							foreach ( $lang_group as $langs ) :
								$title = $langs['title'];
								$title_mobile = $langs['title_mobile'] ? $langs['title_mobile'] : $title;
								$colums = $langs['colums'] ? $langs['colums'] : false;
								$languages = $langs['languages'];
								?>
								<div class="lang__item">
									<?php if ( $title || $title_mobile ) : ?>
										<div class="lang__group-title d-none d-md-block">
											<?php echo $title ? $title : $title_mobile; ?>
										</div>
										<?php if ( $title_mobile ) : ?>
											<div class="lang__group-title d-inline-block d-md-none">
												<?php echo $title_mobile; ?>
											</div>
										<?php endif; ?>
									<?php endif; ?>
									<?php if ( $langs ) : ?>
										<div class="lang__list" <?= $colums ? 'style="column-count:' . $colums . '"' : ''; ?>>
											<?php foreach ( $languages as $item ) :
												$icon = $item['icon'];
												$country = $item['country'];
												$country_mobile = $item['country_mobile'];
												$lang = json_decode( $item['language'][0] );
												if (! empty(parse_url( $lang->url )['query']) ) {
													$lang_url = '?' . parse_url( $lang->url )['query'];
												} else {
													$lang_url = '';
												}
												$code = $lang->language_code;
												$icon_url = $lang->country_flag_url;
												$country = $country ? $country : $code;
												$country_mobile = $country_mobile ? $country_mobile : $code;
												?>
												<?php if ( $lang ) : ?>
													<div class="lang__list-item">
													<a href="<?= $lang_url; ?>" class="lang__link">
														<?php if ( $icon ) : ?>
															<div class="lang__icon-wrap">
																<?php
																if ( $icon ) {
																	$img = wp_get_attachment_image(
																		$icon['id'],
																		'lang_link_icon',
																		false,
																		array(
																			'class' => 'lang__link-icon',
																			'title' => $icon['title'],
																			'alt'   => $icon['alt'],
																		)
																	);
																	echo $img;
																}
																?>
															</div>
														<?php else : ?>
															<div class="lang__icon-wrap">
																<img src="<?= $icon_url; ?>" class="lang__link-icon"
																	alt="<?= $code; ?>">
															</div>
														<?php endif; ?>
														<?php if ( $country || $country_mobile ) : ?>
															<div class="lang__link-country d-none d-md-block">
																<?php echo $country ? $country : $country_mobile; ?>
															</div>
															<?php if ( $country_mobile ) : ?>
																<div class="lang__link-country d-block d-md-none">
																	<?php echo $country_mobile ? $country_mobile : $country; ?>
																</div>
															<?php endif; ?>
														<?php endif; ?>
													</a>
												</div>
											<?php endif; ?>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>