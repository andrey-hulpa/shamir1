<?php
$pro              = get_field( 'pro_show', 6 );
$pro_text         = get_field( 'pro_text', 6 );
$pro_button       = get_field( 'pro_button', 6 );
$pro_image        = get_field( 'pro_image', 6 );
$pro_image_mobile = get_field( 'pro_image_mobile', 6 );
?>
<?php if ( $pro ) :
	$delay = 0;
	?>
	<div class="section section--pro section--white pro">
		<?php
		$class = '';
		if ( $pro_image_mobile ) {
			$class = ' d-none d-md-block ';
			$img   = wp_get_attachment_image(
				$pro_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $pro_image_mobile['title'],
					'alt'   => $pro_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $pro_image ) {
			$img = wp_get_attachment_image(
				$pro_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $pro_image['title'],
					'alt'   => $pro_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container text-center text-lg-left">
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="pro__container">
						<?php if ( $pro_text ) : ?>
							<div data-aos-duration="1500" data-aos="fade-right">
								<?php _e( $pro_text, '_s' ); ?>
							</div>
						<?php endif; ?>
						<?php if ( $pro_button ) :
							$delay += 1000; ?>
							<a data-aos-delay="<?php echo $delay; ?>" data-aos="fade-up" data-aos-duration="500"
							   target="<?php esc_attr_e( $pro_button['target'], '_s' ); ?>"
							   href="<?php echo esc_url( $pro_button['url'] ); ?>"
							   title="<?php esc_attr_e( $pro_button['title'], '_s' ); ?>"
							   class="btn btn--primary"><?php _e( $pro_button['title'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>