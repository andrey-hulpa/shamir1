<?php
//Template name: Technology page
get_header( 'page' );

$technologies_arguments = array(
	'post_type'      => 'technologies',
	'posts_per_page' => - 1
);

$technologies = new WP_Query( $technologies_arguments );

$filter_options = get_field('technology_filter', 'option');
?>
	<div class="technologies technologies--page">
		<div class="container">
			<div style="margin: -5em 0 5em" class="bread">
				<div class="container">
					<div class="bread-wrap">
						<a href="/" class="bread-wrap__item">Home</a>
						<div class="bread-wrap__separator">></div>
						<a href="#" class="bread-wrap__item">Professionals</a>
						<div class="bread-wrap__separator">></div>
						<span class="bread-wrap__item">Technologies</span>
					</div>
				</div>
			</div>

			<div class="technologies__filter">
				Filter By
                <select class="technologies__select js-technologies__filter">
                    <option value="*">All Products</option>
                    <?php
                        if(!empty($filter_options) && is_array($filter_options)) {
                            foreach ($filter_options as $filter_group) {

                                $group_title = !empty($filter_group['group_title']) ? $filter_group['group_title'] : '';

                                echo '<optgroup label="' . esc_attr($group_title) . '">';

                                $products = !empty($filter_group['product_posts']) ? $filter_group['product_posts'] : '';

                                if(!empty($products) && is_array($products)){
                                    foreach ($products as $product) {
                                        $product_technologies = get_field( 'technology', $product->ID );
                                        if ( $product_technologies != null ) {
                                            $technologies_name = '';
                                            foreach ( $product_technologies as $product_technology ) {
                                                $technologies_name .= "." . $product_technology->post_title . ", ";
                                            }
                                        } else {
                                            $technologies_name = '*, ';
                                        }
                                        $technologies_name = substr_replace( $technologies_name, "", - 2 );

                                        printf('<option value="%s">%s</option>',
                                            strtolower( str_replace( '™', '', str_replace( ' ', '', $technologies_name ) ) ),
                                            esc_html( $product->post_title )
                                        );
                                    }
                                }

                                echo '</optgroup>';
                            }
                        }
                    ?>
                </select>
			</div>
			<div class="technologies__grid">
				<?php
				$counter = 1;
				if ( $technologies->have_posts() ):
					while ( $technologies->have_posts() ): $technologies->the_post();
						get_template_part( 'template-parts/technologies-item' );
					endwhile;
					wp_reset_postdata();
				endif; ?>
			</div>
		</div>
	</div>
<?php
get_footer();
