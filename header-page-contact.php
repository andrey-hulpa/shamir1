<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shamir
 */
$object = get_queried_object();
$header_image = get_field('header_image');
$header_image_mobile = get_field('header_image_mobile');
$text_content = get_field('text_content');
if (is_archive()){
	$header_image = get_field( 'header_image', $object->name );
	$header_image_mobile = get_field( 'header_image_mobile', $object->name );
	$text_content = get_field( 'text_content', $object->name );
}
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- PRELOAD FONTS -->
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-bold-aaa-webfont.woff" as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-bold-aaa-webfont.woff2" as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-medium-aaa-webfont.woff"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-medium-aaa-webfont.woff2"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-regular-aaa-webfont.woff"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-regular-aaa-webfont.woff2"
	      as="font">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header">
	<?php get_template_part( 'template-parts/top-bar' ); ?>
	<div class="nav">
		<div class="container d-flex align-items-center justify-content-between">
			<div class="nav__logo logo">
				<?php the_custom_logo(); ?>
			</div>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle nav__burger d-lg-none d-flex align-items-center justify-content-center"
				        aria-controls="primary-menu" aria-expanded="false">
					<img class="nav__burger-img nav__burger-img--bars" src="<?php echo get_template_directory_uri(); ?>/img/icon-menu.svg"
					     alt="">
					<img class="nav__burger-img nav__burger-img--close" src="<?php echo get_template_directory_uri(); ?>/img/icon-close.svg"
					     alt="">
				</button>
				<div class="menu-header-main-container">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-main',
						'menu_id'        => 'primary-menu',
						// 'menu_class'	 => 'nav__list'
					)
				);
				?>
				</div>
			</nav><!-- #site-navigation -->
		</div>
	</div>
</header>
<?php if ( $header_image && $text_content ) : ?>
	<div class="section section--page-contact section--white d-flex align-items-center">
		<?php
		$class = '';
		if ( $header_image_mobile && $header_image ) {
			$class = 'd-none d-md-block ';
			$img   = wp_get_attachment_image(
				$header_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $header_image_mobile['title'],
					'alt'   => $header_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $header_image ) {
			$img = wp_get_attachment_image(
				$header_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $header_image['title'],
					'alt'   => $header_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<?php if ( $text_content ) : ?>
		<div class="container position-relative">
			<div class="row">
				<div class="col-lg-5 col-md-8 col-12">
					<?php echo $text_content; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
<?php endif; ?>