<?php
// Template name: Contact page
get_header( 'page-contact' );
$contact_group = get_field( 'contact_group' );

$map_locations = array();
$locations_counter = -1;
?>

    <div style="margin-top: 3em" class="bread">
        <div class="container">
            <div class="bread-wrap">
                <a href="/" class="bread-wrap__item">Home</a>
                <div class="bread-wrap__separator">></div>
                <a href="#" class="bread-wrap__item">Professionals</a>
                <div class="bread-wrap__separator">></div>
                <span class="bread-wrap__item">Contact</span>
            </div>
        </div>
    </div>


	<div class="contact">
		<div class="container">
			<div class="contact__headline">
				<?php the_content(); ?>
			</div>
			<?php if ( $contact_group ) : ?>
				<div class="contact__tabs tabs js-tabs">
					<div class="row">
						<div class="col-12 col-md-6">
							<ul class="tabs__header contact__group">
								<?php foreach ( $contact_group as $nav ) :
									$title = $nav['title'];
									$id = preg_replace( '/\s+/', '', strtolower( $nav['title'] ) );
									?>
									<li class="tabs__nav contact__nav">
										<a href="#<?= $id; ?>" class="tabs__link contact__link"><?= $title; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
							<?php foreach ( $contact_group as $nav ) :
								$id = preg_replace( '/\s+/', '', strtolower( $nav['title'] ) );
								$locations = $nav['locations'];
								?>
								<div id="<?= $id; ?>" class="contact__tab">
									<div class="accordion js-accordion">
										<?php foreach ( $locations as $location ) :
											$locations_counter++;
											$title = $location['title'];
											$address = $location['address'];
											$contacts = $location['contacts'];
											$address_text = $address['address'];
											if ($address) $map_locations[] = $address;
											?>
											<?php if ( $title ) : ?>
											<div class="contact__title accordion__title" data-counter="<?= $locations_counter; ?>"><?= $title; ?></div>
										<?php endif; ?>
											<div class="contact__content">
												<?php if ($address_text) : ?>
												<div class="contact__address">
													<?= $address_text; ?>
												</div>
	<?php endif; ?>
												<?php if ( $contacts ) : ?>
													<ul class="contact__list">
														<?php foreach ( $contacts as $contact ) :
															$icon = $contact['icon'];
															$type = $contact['type'];
															$text = $contact[ $type ];
															if ( $type == 'site' ) {
																$type = '';
															} else {
																$type .= ':';
															}
															?>
															<li class="contact__item">
																<a href="<?= $type; ?><?= $text; ?>"
																   class="contact__action">
																	<?php
																	if ( $icon ) {
																		$img = wp_get_attachment_image(
																			$icon['id'],
																			'contact_icon',
																			false,
																			array(
																				'class' => 'contact__icon',
																				'title' => $icon['title'],
																				'alt'   => $icon['alt'],
																			)
																		);
																		echo $img;
																	}
																	?>
																	<?= $text; ?>
																</a>
															</li>
														<?php endforeach; ?>
													</ul>
												<?php endif; ?>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="col-12 col-md-6">
							<div class="acf-map" data-zoom="1.2">
								<?php foreach ($map_locations as $location) :
									$lat = $location['lat'];
									$lng = $location['lng'];
									?>
									<div class="marker" data-lat="<?php echo esc_attr($lat); ?>" data-lng="<?php echo esc_attr($lng); ?>"><?php echo $location['address']; ?></div>
								<?php endforeach; ?>
							</div>
						</div>
						<style type="text/css">
							.acf-map {
								width: 100%;
								height: 440px;
								margin-top: 50px;
								/*pointer-events: none;*/
							}

							   .acf-map img {
								   max-width: inherit !important;
							   }
						</style>
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeAaZzGck-h70Kfsap0jL6VmzTIVtaawc"></script>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

<?php
get_footer();
