<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shamir
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- PRELOAD FONTS -->
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-bold-aaa-webfont.woff" as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-bold-aaa-webfont.woff2" as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-medium-aaa-webfont.woff"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-medium-aaa-webfont.woff2"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-regular-aaa-webfont.woff"
	      as="font">
	<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ploni-regular-aaa-webfont.woff2"
	      as="font">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header">
	<?php get_template_part( 'template-parts/top-bar' ); ?>
	<div class="nav">
		<div class="container d-flex align-items-center justify-content-between">
			<div class="nav__logo logo">
				<?php the_custom_logo(); ?>
			</div>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle nav__burger d-lg-none d-flex align-items-center justify-content-center"
				        aria-controls="primary-menu" aria-expanded="false">
					<img class="nav__burger-img nav__burger-img--bars" src="<?php echo get_template_directory_uri(); ?>/img/icon-menu.svg"
					     alt="">
					<img class="nav__burger-img nav__burger-img--close" src="<?php echo get_template_directory_uri(); ?>/img/icon-close.svg"
					     alt="">
				</button>
				<div class="menu-header-main-container">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-main',
						'menu_id'        => 'primary-menu',
						// 'menu_class'	 => 'nav__list'
					)
				);
				?>
				</div>
			</nav><!-- #site-navigation -->
		</div>
	</div>
</header>