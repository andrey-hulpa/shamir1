<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shamir
 */

$nav  = get_field( 'white', 'options' );
$logo = get_field( 'logo', 'options' );

$copy         = get_field( 'copy', 'options' );
$copy_text    = get_field( 'copy_text', 'options' );
$copy_socials = get_field( 'copy_socials', 'options' );

$design       = get_field( 'design_show', 'options' );
$design_text  = get_field( 'design_text', 'options' );
$design_image = get_field( 'design_image', 'options' );

$design_image = $design_image ? $design_image : get_template_directory_uri() . '/img/titan-logo.svg';

$locations = get_nav_menu_locations();

$subscribe       = get_field( 'subscribe_show', 6 );
$subscribe_title = get_field( 'subscribe_title', 6 );
$subscribe_text  = get_field( 'subscribe_text', 6 );
$subscribe_form  = get_field( 'subscribe_form', 6 );

$partner              = get_field( 'partner_show', 6 );
$partner_text         = get_field( 'partner_text', 6 );
$partner_image        = get_field( 'partner_image', 6 );
$partner_image_mobile = get_field( 'partner_image_mobile', 6 );

?>
<?php if ( $subscribe ) :
	?>
	<div class="section section--subscribe section--white subscribe section--red">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-4 d-flex align-items-center justify-content-md-start justify-content-center">
					<?php if ( $subscribe_title ) : ?>
						<?php _e( $subscribe_title, '_s' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-md-6 col-lg-8 flex-column flex-md-row d-flex align-items-center justify-content-md-end justify-content-center">
					<?php if ( $subscribe_text ) : ?>
						<?php _e( $subscribe_text, '_s' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $partner ) :
	?>
	<div class="section section--partner section--white partner">
		<?php
		$class = '';
		if ( $partner_image_mobile ) {
			$class = ' d-none d-md-block ';
			$img   = wp_get_attachment_image(
				$partner_image_mobile['id'],
				'section',
				false,
				array(
					'class' => 'section__image section__image--mobile d-block d-md-none',
					'title' => $partner_image_mobile['title'],
					'alt'   => $partner_image_mobile['alt'],
				)
			);
			echo $img;
		}
		if ( $partner_image ) {
			$img = wp_get_attachment_image(
				$partner_image['id'],
				'section',
				false,
				array(
					'class' => $class . 'section__image',
					'title' => $partner_image['title'],
					'alt'   => $partner_image['alt'],
				)
			);
			echo $img;
		}
		?>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-5">
					<?php if ( $partner_text ) : ?>
						<?php _e( $partner_text, '_s' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ( $nav ) : ?>
	<footer class="footer footer--white d-none d-md-block">
		<div class="container d-flex align-items-center justify-content-md-start justify-content-center flex-wrap flex-lg-nowrap">
			<div class="footer__logo">
				<a href="<?php get_home_url(); ?>" class="footer__logo-link">
					<?php
					if ( $logo ) {
						$img = wp_get_attachment_image(
							$logo['id'],
							'section',
							false,
							array(
								'class' => 'footer_img',
								'title' => $logo['title'],
								'alt'   => $logo['alt'],
							)
						);
						echo $img;
					}
					?>
				</a>
			</div>
			<div class="footer__nav">
				<?php
				$menu = wp_get_nav_menu_object( $locations['menu-footer-1'] );
				echo '<div class="footer__name">' . $menu->name . '</div>';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer-1',
						'container'      => '',
						'menu_class'     => 'footer__list',
						'depth'          => 1
					)
				);
				?>
			</div>
			<div class="footer__nav">
				<?php
				$menu = wp_get_nav_menu_object( $locations['menu-footer-2'] );
				echo '<div class="footer__name">' . $menu->name . '</div>';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer-2',
						'container'      => '',
						'menu_class'     => 'footer__list footer__list--two-columns',
						'depth'          => 1
					)
				);
				?>
			</div>
			<div class="footer__nav">
				<?php
				$menu = wp_get_nav_menu_object( $locations['menu-footer-3'] );
				echo '<div class="footer__name">' . $menu->name . '</div>';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer-3',
						'container'      => '',
						'menu_class'     => 'footer__list',
						'depth'          => 1
					)
				);
				?>
			</div>
			<div class="footer__nav">
				<?php
				$menu = wp_get_nav_menu_object( $locations['menu-footer-4'] );
				echo '<div class="footer__name">' . $menu->name . '</div>';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer-4',
						'container'      => '',
						'menu_class'     => 'footer__list',
						'depth'          => 1
					)
				);
				?>
			</div>
			<div class="footer__nav">
				<?php
				$menu = wp_get_nav_menu_object( $locations['menu-footer-5'] );
				echo '<div class="footer__name">' . $menu->name . '</div>';
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer-5',
						'container'      => '',
						'menu_class'     => 'footer__list',
						'depth'          => 1
					)
				);
				?>
			</div>
		</div>
	</footer>
<?php endif; ?>
<?php if ( $copy ) : ?>
	<footer class="footer footer--copy">
		<div class="container d-md-flex align-items-center justify-content-center text-center">
			<?php if ( $copy_text ) : ?>
				<?php _e( $copy_text, '_s' ); ?>
			<?php endif; ?>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-copy',
					'menu_class'     => 'footer__copy-nav justify-content-center',
					'container'      => ''
				)
			);
			?>
			<?php if ( $copy_socials ) : ?>
				<ul class="social">
					<?php foreach ( $copy_socials as $social ) : ?>
						<li class="social__item">
							<a href="<?php echo esc_url( $social['link'] ); ?>" target="_blank" class="social__link">
								<?php echo $social['icon']; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</footer>
<?php endif; ?>
<?php if ( $design ) : ?>
	<div class="footer-design">
		<div class="container d-flex align-items-center justify-content-center">
			<?php if ( $design_text ) : ?>
				<p class="footer-design__text">
					<?php _e( $design_text, '_s' ); ?>
				</p>
			<?php endif; ?>
			<img class="footer-design__img" src="<?php echo esc_url( $design_image ); ?>" alt="design">
		</div>
	</div>
<?php endif; ?>
<?php wp_footer(); ?>

</body>
</html>
